package org.communiquons.sanitarypassport

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
        setContentView(R.layout.activity_main)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onStart() {
        super.onStart()
        reload()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_add) {
            addNewPassport()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun addNewPassport() {
        IntentIntegrator(this).apply {
            setPrompt(getString(R.string.scan_message))
            initiateScan()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                return
            } else {
                saveNewPassport(result.contents)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun saveNewPassport(url: String) {
        try {
            CertificatesList.addCertificate(this, PassCertificate(url))

            Toast.makeText(this, R.string.success_add_certificate, Toast.LENGTH_SHORT).show()
            reload()
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, R.string.err_failed_add_certificate, Toast.LENGTH_SHORT).show()
        }
    }

    private fun reload() {
        val certificatesList = CertificatesList.getCertificatesList(this).toTypedArray()
        noCertificateNotice.visibility = if (certificatesList.isEmpty()) View.VISIBLE else View.GONE

        mainRecyclerView.layoutManager = LinearLayoutManager(this)
        mainRecyclerView.adapter =
            CertificatesAdapter(
                this,
                certificatesList
            ) { i, s -> onMenuClick(i, s) }
        (mainRecyclerView.adapter as CertificatesAdapter).notifyDataSetChanged()
    }

    private fun onMenuClick(i: PassCertificate, action: Int) {
        if (action == R.id.action_delete) {
            deleteCertificate(i)
        }
    }

    private fun deleteCertificate(i: PassCertificate) {
        AlertDialog.Builder(this)
            .setMessage(R.string.confirm_delete_certificate_message)
            .setPositiveButton(R.string.confirm_delete_certificate_positive) { _, _ ->
                CertificatesList.removeCertificate(
                    this,
                    i
                ); reload()
            }
            .setNegativeButton(R.string.confirm_delete_certificate_negative) { _, _ -> }
            .show()
    }
}