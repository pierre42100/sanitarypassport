package org.communiquons.sanitarypassport

import android.content.Context
import androidx.preference.PreferenceManager
import java.util.*
import java.util.stream.Collectors

private const val PREF_KEY = "certs"

class CertificatesList {
    companion object {

        fun getCertificatesList(ctx: Context): MutableSet<PassCertificate> {
            return PreferenceManager.getDefaultSharedPreferences(ctx)
                .getStringSet(PREF_KEY, TreeSet())!!
                .stream()
                .map { k -> PassCertificate(k) }
                .collect(Collectors.toSet())
        }

        private fun saveCertificatesList(ctx: Context, list: Set<PassCertificate>) {
            PreferenceManager.getDefaultSharedPreferences(ctx)
                .edit()
                .putStringSet(
                    PREF_KEY,
                    list.stream().map { s -> s.url }.collect(Collectors.toSet())
                )
                .apply();
        }

        fun addCertificate(ctx: Context, cert: PassCertificate) {
            val list = getCertificatesList(ctx);
            list.add(cert)
            saveCertificatesList(ctx, list)
        }

        fun removeCertificate(ctx: Context, cert: PassCertificate) {
            val list = getCertificatesList(ctx);
            list.remove(cert)
            saveCertificatesList(ctx, list)
        }
    }
}