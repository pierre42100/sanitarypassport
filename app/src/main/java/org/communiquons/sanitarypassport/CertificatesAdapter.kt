package org.communiquons.sanitarypassport

import android.content.Context
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.forEach
import androidx.recyclerview.widget.RecyclerView
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder

class CertificatesAdapter(
    private val ctx: Context,
    private val dataSet: Array<PassCertificate>,
    private val onMenuClick: (p: PassCertificate, i: Int) -> Unit
) :
    RecyclerView.Adapter<PassCertHolder>() {

    private val barcodeEncoder = BarcodeEncoder()

    private val qrCodeSize by lazy {
        ctx.resources.getDimension(R.dimen.qr_code_fullscreen_size).toInt()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PassCertHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_certificate, parent, false)

        return PassCertHolder(view, onMenuClick).apply { itemView.isLongClickable = true }

    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: PassCertHolder, position: Int) {

        val cert = dataSet[position]


        val bitmap = barcodeEncoder.encodeBitmap(
            cert.cert.value,
            BarcodeFormat.DATA_MATRIX,
            qrCodeSize,
            qrCodeSize
        )

        holder.qrCode.setImageBitmap(bitmap)
        holder.textView.text = cert.getMessage(ctx)
        holder.passCertificate = cert
    }

}


class PassCertHolder(
    view: View,
    private val onMenuClick: (p: PassCertificate, i: Int) -> Unit
) : RecyclerView.ViewHolder(view), View.OnCreateContextMenuListener {
    val textView: TextView = view.findViewById(R.id.textView)
    val qrCode: ImageView = view.findViewById(R.id.qrCode)
    var passCertificate: PassCertificate? = null;

    init {
        qrCode.setOnCreateContextMenuListener(this)
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        MenuInflater(qrCode.context).inflate(R.menu.menu_certificate, menu);

        menu?.forEach { it ->
            it.setOnMenuItemClickListener {
                onMenuClick(passCertificate!!, it.itemId)
                false
            }
        }
    }
}