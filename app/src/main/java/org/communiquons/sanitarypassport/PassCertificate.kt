package org.communiquons.sanitarypassport

import android.annotation.SuppressLint
import android.content.Context
import org.communiquons.sanitarypassport.from_tous_anti_covid.SanitaryCertificate
import org.communiquons.sanitarypassport.from_tous_anti_covid.VaccinationCertificate
import org.communiquons.sanitarypassport.from_tous_anti_covid.WalletCertificate
import org.communiquons.sanitarypassport.from_tous_anti_covid.certificateFromValue
import java.net.URLDecoder
import java.text.SimpleDateFormat

const val PASS_PREFIX = "https://bonjour.tousanticovid.gouv.fr/app/wallet?v="

class PassCertificate(val url: String) : Comparable<PassCertificate> {

    val cert: WalletCertificate

    init {

        val parsed = certificateFromValue(
            URLDecoder.decode(url, "UTF-8")
                .replace(PASS_PREFIX, "")
        )
            ?: throw Exception("Unsupported certificate format!")

        parsed.parse()
        cert = parsed

    }

    override fun equals(other: Any?): Boolean {
        if (other is PassCertificate)
            return url == other.url
        return false
    }

    override fun hashCode(): Int {
        return url.hashCode()
    }

    fun getMessage(ctx: Context): CharSequence? {
        if (isTestCertificate) {
            val cert = testCertificate

            return "${cert.firstName} ${cert.name}\n TEST (PCR / Sérologique / Antigénique)\nLe $dateStr \n${
            if (cert.testResult.equals(
                    "N"
                )
            ) "Négatif" else "Positif"}"
        }

        if (isVaccinationCertificate) {
            val cert = vaccinationCertificate
            return "${cert.firstName} ${cert.name}\nVaccin ${cert.vaccineName}\n${dateStr}\n${cert.lastVaccinationStateRank}° injection"
        }

        return "Document inconnu"
    }

    val isTestCertificate get() = cert is SanitaryCertificate

    val testCertificate get() = cert as SanitaryCertificate

    val isVaccinationCertificate get() = cert is VaccinationCertificate

    val vaccinationCertificate get() = cert as VaccinationCertificate

    val time: Long?
        get() {
            return if (isTestCertificate) {
                testCertificate.analysisDate
            } else {
                vaccinationCertificate.lastVaccinationDate?.time
            }
        }

    val dateStr: String
        @SuppressLint("SimpleDateFormat") get() {
            val analysisDateFormat = SimpleDateFormat("d MMM yyyy, HH:mm")
            return time?.let { analysisDateFormat.format(it) } ?: "N/A"
        }

    override fun compareTo(other: PassCertificate): Int {
        val otherTime = if (other.time != null) other.time!! else 0;
        return otherTime.compareTo(if (time != null) time!! else 0)
    }
}