package org.communiquons.sanitarypassport.from_tous_anti_covid

class WalletCertificateMalformedException(message: String = "Invalid certificate code") :
    Exception(message)