package org.communiquons.sanitarypassport.from_tous_anti_covid

private fun extractCertificateType(value: String): WalletCertificateType? = WalletCertificateType.values()
    .firstOrNull { it.validationRegexp.matches(value) }

public fun certificateFromValue(value: String): WalletCertificate? {
    val type: WalletCertificateType = extractCertificateType(value) ?: return null
    return when (type) {
        WalletCertificateType.SANITARY -> SanitaryCertificate(value)
        WalletCertificateType.VACCINATION -> VaccinationCertificate(value)
    }
}